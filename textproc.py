import nltk
import networkx as nx
import matplotlib
from nltk.corpus import wordnet as wn
from nltk.util import ngrams
from nltk.tokenize import word_tokenize
from nltk.stem import PorterStemmer
from nltk.corpus import stopwords
import numpy as np
import regex as re
import string
from collections import Counter
import datetime
import pandas as pd
from wordcloud import WordCloud


stemming = PorterStemmer()


stops = set(stopwords.words("english"))
stopwords = set(stopwords.words("english"))

def getstop_words():
    stop_words = set(nltk.corpus.stopwords.words('english'))
    add_stop = ['hey','take', "ca", "'m", "'s", "n't", 'like', 'would', '.', '?', '!', ',', 'hi', 'yesterday', 'get', 'make', 'hello', 'last', 'want' , 'night', 'need', 'ally', '#', '%', 'used', 'got', 'thanks', 'tried' ,'1', '2', '3', '4', '5', '6', '7', '8', '9', '0']
    # words to add back in from stopwords
    keep_stop = ['how', 'can']

    #add/remove stopwords
    #stop_words |= add_stop
    #stop_words -= keep_stop
    for item in add_stop:
        stop_words.add(item)
    for item in keep_stop:
          stop_words.remove(item)
    return stop_words

       
            
def plural(word):
    if word.endswith('y'):
        return word[:-1] + 'ies'
    elif word[-1] in 'sx' or word[-2:] in ['sh', 'ch']:
        return word + 'es'
    elif word.endswith('an'):
        return word[:-2] + 'en'
    else:
        return word + 's'

def unusual_words(text):
  text_vocab = set(w.lower() for w in text if w.isalpha())
  english_vocab = set(w.lower() for w in nltk.corpus.words.words())
  unusual = text_vocab.difference(english_vocab)
  return sorted(unusual)

#Let's define a function to compute what fraction of words in a text are not in the stopwords list:
def content_fraction(text):
   stopwords = nltk.corpus.stopwords.words('english')
   content = [w for w in text if w.lower() not in stopwords]
   return len(content) / len(text)

#As our final example, we define a function to extract 
#the stress digits and then scan our lexicon to find words having a particular stress pattern.
def stress(pron):
  return [char for phone in pron for char in phone if char.isdigit()]

def supergloss(s):
  definitions=s.definition();
  for hypo in s.hyponyms():
    definitions+="\n"+hypo.definition()
    for hyper in s.hypernyms():
      definitions+="\n"+hyper.definition()
  return definitions
  
def count_syllables(pron):
    return len([w for w in pron if re.findall("[aeiou]",w.lower())])
  
def hedge(text):
    # test = 'this is a test sentence to insert like after every third word'.split()
    ids = [index-1 for index in list(range(3,len(text)+1,3))]
    for id in ids:
        text.insert(id,'like')
    return text
  
def zipfs_law(text,n):
    text_fd=nltk.FreqDist(text)
    text_fd_common=text_fd.most_common(n)
    freqs=[y for x,y in text_fd_common]
    ranks=[1/freq for freq in freqs]
    pyplot.plot(ranks,freqs)
    
def generate_model(text, n):
    text_fd=nltk.FreqDist(text)
    text_fd_common=text_fd.most_common(n)
    rand_words = [word for word,index in text_fd_common]
    return rand_words
  
def generate_sentence(text):
    start_words = set(word for word in brown_romance_rand if word.istitle())
    punc_symbols = set(word for word in brown_romance_rand if not word.isalpha() and len(word) == 1)
    other_words = set(brown_romance_rand).difference(punc_symbols)
    other_words = list(other_words)
    start_words = list(start_words)
    punc_symbols = list(punc_symbols)
    limit_1 = random.randrange(1, len(other_words))
    limit_2 = random.randrange(1, len(other_words))

    if limit_1 < limit_2:
        rand_indices = list(range(limit_1, limit_2))
        other_indexes = [random.choice(rand_indices) for id in rand_indices]
        wordlist = [other_words[id] for id in other_indexes]
    else:
        rand_indices = list(range(limit_2, limit_1))
        other_indexes = [random.choice(rand_indices) for id in rand_indices]
        wordlist = [other_words[id] for id in other_indexes]

    wordlist.insert(0,random.choice(start_words))
    wordlist.append(random.choice(punc_symbols))
    print(" ".join(wordlist))

#Finding Word Stems
def stem(word):
  regexp = r'^(.*?)(ing|ly|ed|ious|ies|ive|es|s|ment)?$'
  stem, suffix = re.findall(regexp, word)[0]
  return stem

#Word Segmentation example
def segment(text, segs):
  words = []
  last = 0
  for i in range(len(segs)):
    if segs[i] == '1':
      words.append(text[last:i+1])
      last = i+1
      words.append(text[last:])
      print(words)
    return words
  
def evaluate(text, segs):
  words = segment(text, segs)
  text_size = len(words)
  lexicon_size = sum(len(word) + 1 for word in set(words))
  return text_size + lexicon_size

def freq_words(file, min=1, num=10):
  text = open(file).read()
  tokens = word_tokenize(text)
  freqdist = nltk.FreqDist(t for t in tokens if len(t) >= min)
  return freqdist.most_common(num)



def traverse(graph, start, node):
  graph.depth[node.name] = node.shortest_path_distance(start)
  for child in node.hyponyms():
    graph.add_edge(node.name, child.name) [1]
    traverse(graph, start, child) [2]

def hyponym_graph(start):
    G = nx.Graph() [3]
    G.depth = {}
    traverse(G, start, start)
    return G

def graph_draw(graph):
    nx.draw_graphviz(graph,
         node_size = [16 * graph.degree(n) for n in graph],
         node_color = [graph.depth[n] for n in graph],
         with_labels = False)
    matplotlib.pyplot.show()
    
def n_grams(text, n):
  token = nltk.word_tokenize(text)
  grams = ngrams(token, n)
  return grams

def bi_grams(text):
  token = nltk.word_tokenize(text)
  grams = ngrams(token, 2)
  return grams

def tri_grams(text):
  token = nltk.word_tokenize(text)
  grams = ngrams(token, 3)
  return grams

#ere we will also strip out non alphanumeric words/characters (such as numbers and punctuation) 
#using .isalpha (you could use .isalnum if you wanted to keep in numbers as well).
def identify_tokens(row):
  review = row['user_utterance']
  tokens = nltk.word_tokenize(review)
  # taken only words (not punctuation)
  token_words = [w for w in tokens if w.isalpha()]
  return token_words

def identify_tokensa(row):
  review = row['text']
  tokens = nltk.word_tokenize(review)
  # taken only words (not punctuation)
  token_words = [w for w in tokens if w.isalpha()]
  return token_words

def identify_tokensb(row):
  review = row['article_text']
  tokens = nltk.word_tokenize(review)
  # taken only words (not punctuation)
  token_words = [w for w in tokens if w.isalpha()]
  return token_words

def identify_tokensc(row):
  review = row['user_utterance']
  tokens = nltk.word_tokenize(review)
  # taken only words (not punctuation)
  token_words = [w for w in tokens if w.isalpha()]
  return token_words

def identify_sent_tokens(row):
  review = row['text2']
  tokens = nltk.sent_tokenize(review)
  # taken only words (not punctuation)
  token_sents = [w for w in tokens if w.isalpha()]
  return token_sents
  
#Stem to create stem list
def stem_list(row):
  my_list = row['user_utterance']
  stemmed_list = [stemming.stem(word) for word in my_list]
  return (stemmed_list)
  
def stem_list2(row):
  my_list = row['text']
  stemmed_list = [stemming.stem(word) for word in my_list]
  return (stemmed_list)  

def stem_list3(row):
  my_list = row['user_utterance']
  stemmed_list = [stemming.stem(word) for word in my_list]
  return (stemmed_list)  


def remove_stops(row):
  my_list = row['stemmed_words']
  meaningful_words = [w for w in my_list if not w in stops]
  return (meaningful_words)

def remove_stops_txt(row):
    my_list = row['words']
    meaningful_words = [w for w in my_list if not w in stops]
    return (meaningful_words)
  
#Now we will rejoin our meaningful stemmed words into a single string.
def rejoin_words(row):
  my_list = row['stem_meaningful']
  joined_words = ( " ".join(my_list))
  return joined_words

#calculate sentence scores

def cal_sent_scores(word):
  sentence_scores = {}
  for sent in word:
    for word in nltk.word_tokenize(sent.lower()):
      if word in word_frequencies.keys():
        if len(sent.split(' ')) < 30:
          if sent not in sentence_scores.keys():
            sentence_scores[sent] = word_frequencies[word]
          else:
            sentence_scores[sent] += word_frequencies[word]
  return sentence_scores

#
def find_weighted_freq(text):
  for word in nltk.word_tokenize(text):
    if word not in stopwords.words():
      if word not in word_frequencies.keys():
        word_frequencies[word] = 1
      else:
        word_frequencies[word] += 1
  return word_frequencies

#######
''' Text summerazation example functions'''
def read_article(file_name):
    file = open(file_name, "r")
    filedata = file.readlines()
    article = filedata[0].split(". ")
    sentences = []

    for sentence in article:
        print(sentence)
        sentences.append(sentence.replace("[^a-zA-Z]", " ").split(" "))
        sentences.pop() 
    
    return sentences

def sentence_similarity(sent1, sent2, stopwords=None):
    if stopwords is None:
        stopwords = []
 
    sent1 = [w.lower() for w in sent1]
    sent2 = [w.lower() for w in sent2]
 
    all_words = list(set(sent1 + sent2))
 
    vector1 = [0] * len(all_words)
    vector2 = [0] * len(all_words)
 
    # build the vector for the first sentence
    for w in sent1:
        if w in stopwords:
            continue
        vector1[all_words.index(w)] += 1
 
    # build the vector for the second sentence
    for w in sent2:
        if w in stopwords:
            continue
        vector2[all_words.index(w)] += 1
 
    return 1 - cosine_distance(vector1, vector2)
 
def build_similarity_matrix(sentences, stop_words):
    # Create an empty similarity matrix
    similarity_matrix = np.zeros((len(sentences), len(sentences)))
 
    for idx1 in range(len(sentences)):
        for idx2 in range(len(sentences)):
            if idx1 == idx2: #ignore if both are same sentences
                continue 
            similarity_matrix[idx1][idx2] = sentence_similarity(sentences[idx1], sentences[idx2], stop_words)

    return similarity_matrix


def generate_summary(file_name, top_n=5):
    #nltk.download("stopwords")
    stop_words = stopwords.words('english')
    summarize_text = []

    # Step 1 - Read text anc split it
    sentences =  read_article(file_name)

    # Step 2 - Generate Similary Martix across sentences
    sentence_similarity_martix = build_similarity_matrix(sentences, stop_words)

    # Step 3 - Rank sentences in similarity martix
    sentence_similarity_graph = nx.from_numpy_array(sentence_similarity_martix)
    scores = nx.pagerank(sentence_similarity_graph)

    # Step 4 - Sort the rank and pick top sentences
    ranked_sentence = sorted(((scores[i],s) for i,s in enumerate(sentences)), reverse=True)    
    print("Indexes of top ranked_sentence order are ", ranked_sentence)    

    for i in range(top_n):
      summarize_text.append(" ".join(ranked_sentence[i][1]))

    # Step 5 - Offcourse, output the summarize texr
    print("Summarize Text: \n", ". ".join(summarize_text))

def tokenize(text):
  return re.findall(r'[\w-]*\p{L}[\w-]*', text)

##### DEFINE FUNCTIONS #####
''' 
User: kzdx5z
Function: Take multiple utterances from chat bot data and find words that often occur in the same sentence (not bi-grams or tri-grams, but bi-word pairs and tri-word groups)
Progress: Finished 'dumb' logic
To do:      - bucket terms
            - further pre-processing: stems/lemmas
            - relate bi- and tri- words back to dataframe rows (not grams) -> use counter index and search each list for that counter object and append to list of indexes?
            - use scatter view to display
            - auto create the known and unknown intent dataframes - wrap all into a function and run both on init
            - figure out how to download space web en sm (issues doing this) 
'''

def list_smash(text, n=2):
        ##### PRE-PROCESS #####
        #stopwords - will need to add others
        #stop_words = set(stopwords.words('english'))
        stop_words = stopwords
        add_stop = ['take', "ca", "'m", "'s", "n't", 'like', 'would', '.', '?', '!', ',', 'hi', 'yesterday', 'get', 'make', 'hello', 'last', 'want' , 'night', 'need', 'ally', '#', '%', 'used', 'got', 'thanks', 'tried' ,'1', '2', '3', '4', '5', '6', '7', '8', '9', '0']
        # words to add back in from stopwords
        keep_stop = ['how', 'can']

        #add/remove stopwords
        #stop_words |= add_stop
        #stop_words -= keep_stop
        for item in add_stop:
            stop_words.add(item)
        for item in keep_stop:
            stop_words.remove(item)
                
        #tokenize
        listx = word_tokenize(str(text))

        #cleanup
        listx = [word.lower() for word in listx if not word.lower() in stop_words and word.lower() not in string.punctuation]
        #remove dupes
        listx = list(set(listx))
        
        #initialize
        lenlist = len(listx)
        a = 0
        b = 0
        out_list = []

        #two word combo iterator
        if n==2:
            while lenlist > 1:
                for x in listx[a+1:]:
                    out_list.append(sorted([listx[a], x]))
                lenlist = lenlist -1
                a = a + 1

            return out_list
        
        #three word combo iterator
        elif n==3:
            while lenlist > 2:
                for x in listx[a+1:]:
                    for y in listx[a+2:]:
                        out_list.append(sorted([listx[a], x, y]))
                    lenlist = lenlist -1
                    a = a + 1
            return out_list

        else:
            return None
          
################################################33

def remove_stop(tokens):
  #xstopwords = getstop_words()
  return [t for t in tokens if t.lower() not in stopwords]


def prepare_tokens(text, pipeline):
  tokens = text
  for transform in pipeline:
    tokens = transform(tokens)
  return tokens

#create count_word funstion to sort words by freq
def count_words(df, column='tokens', preprocess=None, min_freq=2):

    # process tokens and update counter
    def update(doc):
        tokens = doc if preprocess is None else preprocess(doc)
        counter.update(tokens)

    # create counter and run through all data
    counter = Counter()
    df[column].map(update)

    # transform counter into a DataFrame
    freq_df = pd.DataFrame.from_dict(counter, orient='index', columns=['freq'])
    freq_df = freq_df.query('freq >= @min_freq')
    freq_df.index.name = 'token'

    return freq_df.sort_values('freq', ascending=False)
  
#wordcloud function
def wordcloud(word_freq, title=None, max_words=200, stopwords=None):

    wc = WordCloud(width=800, height=400,
                   background_color= "black", colormap="Paired",
                   max_font_size=150, max_words=max_words)

    # convert DataFrame into dict
    if type(word_freq) == pd.Series:
        counter = Counter(word_freq.fillna(0).to_dict())
    else:
        counter = word_freq

    # filter stop words in frequency counter
    if stopwords is not None:
        counter = {token:freq for (token, freq) in counter.items()
                              if token not in stopwords}
    wc.generate_from_frequencies(counter)

    plt.title(title)

    plt.imshow(wc, interpolation='bilinear')
    plt.axis("off")

#Ngram
def ngrams(tokens, n=2, sep=' ', stopwords=set()):
    return [sep.join(ngram) for ngram in zip(*[tokens[i:] for i in range(n)])
            if len([t for t in ngram if t in stopwords])==0]


#Ranking with TF-IDF ***********************************
def compute_idf(df, column='tokens', preprocess=None, min_df=2):

    def update(doc):
        tokens = doc if preprocess is None else preprocess(doc)
        counter.update(set(tokens))

    # count tokens
    counter = Counter()
    df[column].map(update)

    # create DataFrame and compute idf
    idf_df = pd.DataFrame.from_dict(counter, orient='index', columns=['df'])
    idf_df = idf_df.query('df >= @min_df')
    idf_df['idf'] = np.log(len(df)/idf_df['df'])+0.1
    idf_df.index.name = 'token'
    return idf_df
  
#Creating Frequency Timelines
def count_keywords(tokens, keywords):
    tokens = [t for t in tokens if t in keywords]
    counter = Counter(tokens)
    return [counter.get(k, 0) for k in keywords]

def count_keywords_by(df, by, keywords, column='tokens'):
    freq_matrix = df[column].apply(count_keywords, keywords=keywords)
    freq_df = pd.DataFrame.from_records(freq_matrix, columns=keywords)
    freq_df[by] = df[by] # copy the grouping column(s)
    return freq_df.groupby(by=by).sum().sort_values(by)

